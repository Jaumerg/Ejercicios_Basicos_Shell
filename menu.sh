#!/bin/bash

options=("Ejercicio 1" "Ejercicio 2" "Ejercicio 3" "Ejercicio 4" "Ejercicio 5" "Ejercicio 6" "Ejercicio 7" "Ejercicio 8" "Salir")

select opt in "${options[@]}"
do
	case $opt in
		"Ejercicio 1")
			echo "Introduce todos los ficheros que quieres analizar:"
			read linea
			cd scripts/ej1
			./ej1.sh $linea
			cd ..
			cd ..
			;;
		"Ejercicio 2")
			echo "Introduce el numero de niveles de la piramide:"
			read linea
			./scripts/ej2/ej2.sh $linea
			;;
		"Ejercicio 3")
			echo "Introduce el nombre del fichero a tratar:"
			read linea
			cd scripts/ej3
			./ej3.sh $linea
			cd ..
			cd ..
			;;
		"Ejercicio 4")
			./scripts/ej4/ej4.sh
			;;
		"Ejercicio 5")
			./scripts/ej5/ej5.sh
			;;
		"Ejercicio 6")
			cd scripts/ej6
			./ej6.sh
			cd ..
			cd ..
			;;
		"Ejercicio 7")
			cd scripts/ej7
			./ej7.sh
			cd ..
			cd ..
			;;
		"Ejercicio 8")
			./scripts/ej8/ej8.sh
			;;
		"Salir")
			break
			;;
		*) echo "Opción invalida";;
	esac
done

