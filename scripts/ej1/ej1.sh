#!/bin/bash

echo "Fichero			Mediana"
echo "-------			-------"

for fichero in $* 
do
	if [ -f $fichero ]; then
		#sort -o $fichero $fichero
		sort -n -o $fichero $fichero
		long=$(cat $fichero | wc -l)
		let resto=$long%2
		#es par
		if [ $resto -eq 0 ]; then
			let pos1=$long/2
			let pos2=$long/2+1
			num1=$(head -$pos1 $fichero | tail -1)
			num2=$(head -$pos2 $fichero | tail -1)
			med=$(echo "scale=2; ($num1 + $num2) / 2" | bc)
			echo "$fichero		 $med"
		else 
			#impar
			let nummed=$long/2+1
			med=$(head -$nummed $fichero | tail -1)
			echo "$fichero		$med"
		fi
	fi
done
echo "-------			-------"
