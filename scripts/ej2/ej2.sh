#!/bin/bash

if [ $# = 1 ] && (( $1>0 )); then
	let num=$1-1
	numAst=1
	asterisco='*'
	espacio=' '
	for i in `seq 1 $1`
	do
		linea=""
		#ponemos todos los espacios
		for x in `seq 1 $num`
		do	
			linea=$linea$espacio
		done
		#ponemos todos los asteriscos
		for y in `seq 1 $numAst`
		do
			linea=$linea$asterisco
		done
		echo "$linea"
		let numAst=$numAst+2
		let num=$num-1
	done
else
	echo "Error"
fi
