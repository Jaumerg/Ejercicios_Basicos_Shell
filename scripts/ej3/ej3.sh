#!/bin/bash

cat $1 | sed 's/[^a-zA-Z ]*//g' | tr [:upper:] [:lower:] | while read line; do echo $line | sed 's/ /\n/g' | sort | awk '{line=line " " $0}END {print line}' ; done
