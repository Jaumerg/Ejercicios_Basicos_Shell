#!/bin/bash

echo "Su combinacion para la quiniela es:"

for i in `seq 1 14`;
do
	let number=$RANDOM%3
	if [ $number -eq 0 ]; then
		echo $i.- X
	else
		echo $i.- $number
	fi
done
