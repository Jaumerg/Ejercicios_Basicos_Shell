#!/bin/bash

>palabras.txt
echo "Introduzca palabras, escriba salir para terminar"
let counter=0
read linea 
is_number='^[0-9]+$'
while [ $linea != "salir" ]; do
	if ! [[ $linea =~ $is_number ]]; then
		let counter=$counter+1
		echo $linea >> palabras.txt
	else
		head -n $linea palabras.txt | tail -n 1
	fi
	read linea
done
echo "El numero de palabras es:" $counter
echo "Tenga un buen dia."
