#!/bin/bash

ps -eo euser --sort user > user.txt
ps -eo vsz --sort user > vsz.txt

act_user=$(head -n 2 user.txt | tail -n 1)
act_vsz=$(head -n 2 vsz.txt | tail -n 1)
let long_file=$(cat user.txt | wc -l)

echo "USER 			VSZ"
for i in `seq 3 $long_file`
do
	new_user=$(head -n $i user.txt | tail -n 1)
	new_vsz=$(head -n $i vsz.txt | tail -n 1)
	if [[ "$new_user" == "$act_user" ]]; then
		act_vsz=$((act_vsz+new_vsz))
	else
		echo "$act_user			$act_vsz"
		act_user=$new_user
		act_vsz=$new_vsz
	fi
done
rm user.txt
rm vsz.txt
